#include "Transition.h"

Transition::Transition(const std::string & initialState, const std::string & finalState, char charRead, char charWrite, bool leftRight) : initialState(initialState), finalState(finalState), charRead(charRead), charWrite(charWrite), leftRight(leftRight)
{
	// Empty
}

std::ostream& operator << (std::ostream& os, const Transition& trasition)
{
os << "Transition [initialState=" << trasition.initialState << ", finalState=" << trasition.finalState << ", charRead=" << trasition.charRead << ", charWrite=" << trasition.charWrite << ", moveLeftRight=" << boost::lexical_cast<std::string>(trasition.leftRight) << "]" << std::endl;
return os;
}
