#ifndef UTILS_HPP
#define UTILS_HPP
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include "string-utils.h"
#include "string.hpp"

/* Overloads the << Operator to show the contents of a vector */
template <typename T>
std::ostream& operator << (std::ostream& os, const std::vector<T>& myVector)
{
	for (int i = 0; i < myVector.size(); ++i) {
		os << myVector[i];
		if (i != myVector.size() - 1)
			os << ", ";
	}
	return os;
}

/* Transforms a bool to it's string equivalent */
inline std::string boolToString(const bool b)
{
	std::ostringstream ss;
	ss << std::boolalpha << b;
	return ss.str();
}

/* Transforms a string to it's binary equivalent */
inline std::string intToBinaryString(int n)
{
	if (n == 0)
		return "0";
	std::string r;
	while (n != 0) { r = (n % 2 == 0 ? "0" : "1") + r; n /= 2;}
	return r;
}

template <class Container>
inline void split(const std::string& str, Container& cont, const std::string& delims = " ")
{
	//boost::split(cont, str, boost::is_any_of(delims));
	cont = Split(str, delims,true);
}

template <class Container, class ToSearch>
inline int indexOf(Container container, ToSearch toSearch)
{
	typename Container::iterator iterator = std::find(container.begin(), container.end(), toSearch);

	return iterator != container.cend() ? std::distance(container.begin(), iterator) : -1;
}
template <class ToSearch>
inline int indexOf(std::string str, ToSearch toSearch)
{
	std::string::size_type index;
	index = str.find(toSearch);
	return index != std::string::npos ? index : -1;
}

#endif // !UTILS_HPP
