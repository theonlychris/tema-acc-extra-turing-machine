#pragma once
#include "Utils.hpp"

class Transition
{
public:
	Transition() = default;
	Transition(const std::string & initialState, const std::string & finalState, char charRead, char charWrite, bool leftRight);
public:
	friend std::ostream& operator << (std::ostream& os, const Transition& trasition);
public:
	std::string initialState;
	std::string finalState;
	char charRead;
	char charWrite;
	bool leftRight;
};

