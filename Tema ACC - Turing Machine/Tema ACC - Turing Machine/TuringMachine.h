#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <bitset>
#include <fstream>
#include "Transition.h"
#include "Utils.hpp"
class TuringMachine
{
public:
	TuringMachine();
public:
	std::string gStringFromTuring();
	void readInputStringFromFile(const std::string& fileName);
	void gTuringFromString(const std::string & stringToTransform);
	bool verifyTuringMachine();

public:
	friend std::ostream& operator << (std::ostream& os, const TuringMachine& turingMachine);
public:
	std::vector<std::string> states;
	std::string inputAlphabet;
	std::string tapeAlphabet;
	std::vector<Transition> transitions;
	std::string initialState;
	char blank;
	std::vector<std::string> finalStates;
	std::string inputString;
};

