#include <iostream>
#include "TuringMachine.h"

void turingStringExample()
{
	TuringMachine turingMachine;

	/* Reads the TuringMachineString from input file  */
	turingMachine.readInputStringFromFile("turingString.txt");

	std::cout << "Input string: " << turingMachine.inputString << std::endl;

	std::cout << "Turing machine before creation: " << std::endl;
	std::cout << turingMachine << std::endl;

	/* Creates the turing machine from String */
	turingMachine.gTuringFromString(turingMachine.inputString);

	/* Verifies the turing Machine */
	if (!turingMachine.verifyTuringMachine())
	{
		std::cout << "The string is not a turing machine";
		return;
	}

	std::cout << "Turing machine after creation: " << std::endl;
	std::cout << turingMachine << std::endl;

	std::string stringFromTuring = turingMachine.gStringFromTuring();

	std::cout << "Generated string: " << stringFromTuring << std::endl;

	/* Verifies if the inputString is equal to the string generate from turing */
	if (turingMachine.inputString == stringFromTuring)
		std::cout << "The two strings are identical";
	else
		std::cout << "The two strings are different";
}

int main()
{
	turingStringExample();
	std::cin.get();
	return 0;
}


