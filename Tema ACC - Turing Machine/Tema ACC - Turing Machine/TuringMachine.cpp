#include "TuringMachine.h"

TuringMachine::TuringMachine()
{
	//Empty
}

std::string TuringMachine::gStringFromTuring()
{
	std::string finalString;

	/* Adds the states to the string */
	for (std::string state : states)
	{
		finalString += intToBinaryString(boost::lexical_cast<int>(state));
		finalString += '#';
	}

	/* Adds the inputAlphabet to the string */
	finalString += '#';
	for (int i = 0; i < inputAlphabet.size(); ++i)
	{
		finalString += intToBinaryString(inputAlphabet[i]);
		finalString += '#';
	}
	finalString += '#';

	/* Adds the tapeAlphabet to the string */
	for (int i = 0; i < tapeAlphabet.size(); ++i)
	{
		finalString += intToBinaryString(tapeAlphabet[i]);
		finalString += '#';
	}

	finalString += '#';

	/* Adds the initialState to the string */
	finalString += (intToBinaryString(boost::lexical_cast<int>(initialState)));
	finalString += "##";

	/* Adds the blankCharacter to the string */
	finalString += (intToBinaryString((int)blank)); 
	finalString += "##";

	/* Adds the finalStates to the string */
	for (std::string state : finalStates)
	{
		finalString += (intToBinaryString(boost::lexical_cast<int>(state)));
		finalString += '#';
	}
	finalString += '#';

	/* Adds the transitions to the string */
	for (Transition t : transitions)
	{
		finalString += (intToBinaryString(boost::lexical_cast<int>(t.initialState)));
		finalString += '#';
		finalString += (intToBinaryString(t.charRead)); 
		finalString += '#';
		finalString += (intToBinaryString(t.charWrite));
		finalString += '#';
		if (t.leftRight == true)
			finalString += "1#";
		else
			finalString += "0#";
		finalString += (intToBinaryString(boost::lexical_cast<int>(t.finalState)));
		finalString += '#';
		finalString += "#";
	}
	return finalString;
}

void TuringMachine::readInputStringFromFile(const std::string& fileName)
{
	std::ifstream myFile("turingString.txt");
	myFile >> inputString;

	/* Closing the file */
	myFile.close();
}

void TuringMachine::gTuringFromString(const std::string & stringToTransform)
{
	std::vector<std::string> parsedStrings;
	split(stringToTransform, parsedStrings, "##");

	if (parsedStrings.size() < 6)
		return;

	std::vector<std::string> tokens;
	split(parsedStrings[0], tokens, "#");
	states.clear();

	/* Adds the states to the Turing Machine */
	for (std::string s : tokens)
		states.push_back(boost::lexical_cast<std::string>(std::stoi(s, nullptr, 2)));
	inputAlphabet = "";

	split(parsedStrings[1], tokens, "#");

	/* Adds the inputAlphabet to the Turing Machine */
	for (std::string s : tokens)
	{
		inputAlphabet += ((char)std::stoi(s, nullptr, 2));
	}
	tapeAlphabet = "";

	split(parsedStrings[2], tokens, "#");

	/* Adds the tapeAlphabet to the Turing Machine */
	for (std::string s : tokens)
	{
		tapeAlphabet += ((char)std::stoi(s, nullptr, 2));
	}
	/* Adds the initialState to the Turing Machine */
	initialState = boost::lexical_cast<std::string>(std::stoi(parsedStrings[3], nullptr, 2));
	/* Adds the blankChar to the Turing Machine */
	blank = (char)std::stoi(parsedStrings[4], nullptr, 2);
	finalStates.clear();
	split(parsedStrings[5], tokens, "#");
	/* Adds the finalStates to the Turing Machine */
	for (std::string s : tokens)
		finalStates.push_back(boost::lexical_cast<std::string>(std::stoi(s, nullptr, 2)));
	transitions.clear();
	/* Adds the transitions to the Turing Machine */
	for (int i = 6; i < parsedStrings.size(); i++)
	{
		Transition t;
		split(parsedStrings[i], tokens, "#");
		if (tokens.size() != 5)
			return;
		t.initialState = boost::lexical_cast<std::string>(std::stoi(tokens[0], nullptr, 2));
		t.charRead = (char)std::stoi(tokens[1], nullptr, 2);
		t.charWrite = (char)std::stoi(tokens[2], nullptr, 2);
		if (str::contains(tokens[3], "0"))
			t.leftRight = false;
		else
			t.leftRight = true;
		t.finalState = boost::lexical_cast<std::string>(std::stoi(tokens[4], nullptr, 2));
		transitions.push_back(t);
	}
	
	/* Verifies the Turing Machine */
	/*
	return verifyTuringMachine();
	*/
}

bool TuringMachine::verifyTuringMachine()
{
		for (Transition t : transitions)
		{
			if (-1 == indexOf(states, t.initialState) || -1 == indexOf(states, t.finalState) || (-1 == indexOf(tapeAlphabet, t.charRead)) || (indexOf(tapeAlphabet, t.charWrite) == -1))
				return false;
		}

		return true;
}

std::ostream& operator<<(std::ostream& os, const TuringMachine& turingMachine)
{
os 
<< "TuringMachine " << std::endl 
<< "[" << std::endl
<< "states= " << turingMachine.states << std::endl 
<< "inputAlphabet="  << turingMachine.inputAlphabet << std::endl
<< "tapeAlphabet=" << turingMachine.tapeAlphabet << std::endl 
<< "transitions=" << std::endl << turingMachine.transitions << std::endl 
<< "initialState=" << turingMachine.initialState << std::endl 
<< "blank=" << turingMachine.blank << std::endl 
<< "finalStates=" << turingMachine.finalStates << std::endl
<< "]";
return os;
}
